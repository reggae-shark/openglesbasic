//
//  ViewController.h
//  GamesDevAssignment2
//
//  Created by Dalton Danis on 2018-03-07.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vertex.h"

@import GLKit;

@interface ViewController : GLKViewController {
//
    __weak IBOutlet UISwitch *expFog;
    __weak IBOutlet UILabel *expFogLabel;
    __weak IBOutlet UIStepper *fogIntensity;
    __weak IBOutlet UIButton *fogGreen;
    __weak IBOutlet UIButton *fogBlue;
    __weak IBOutlet UIButton *fogOrange;
    GLKVector4 fogColor;
    Vertex *vert;
    long vertexCount;
    
    
}
@end

