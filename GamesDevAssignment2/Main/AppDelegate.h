//
//  AppDelegate.h
//  GamesDevAssignment2
//
//  Created by Dalton Danis on 2018-03-07.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

