//
//  ViewController.m
//  GamesDevAssignment2
//
//  Created by Dalton Danis on 2018-03-07.
//  Copyright © 2018 opengl. All rights reserved.
//



#import "ViewController.h"
#import "Vertex.h"
#import "BaseEffect.h"
#import "Cube.h"
#import "Sphere.h"
#import "ObjModel.h"
#import "ObjLoader.h"

@interface ViewController ()

@end

@implementation ViewController {
    BaseEffect *_shader;
    Cube *_cube;
    Sphere *_sphere;
    ObjModel *_obj;
    NSMutableArray *mazeArray;
    NSInteger array[10][11];
    float xDisp;
    float zDisp;
    float rotateX;
    float rotateY;
    __weak IBOutlet UILabel *MazeLabel;
    __weak IBOutlet UISlider *scaleSlider;
    __weak IBOutlet UISlider *rotXSlider;
    __weak IBOutlet UISlider *rotYSlider;
    __weak IBOutlet UISlider *rotZSlider;
    __weak IBOutlet UISlider *posXSlider;
    __weak IBOutlet UISlider *posYSlider;
    __weak IBOutlet UISlider *posZSlider;
}

- (void)setupScene {
    _shader = [[BaseEffect alloc] initWithVertexShader:@"SimpleVertex.glsl" fragmentShader:@"SimpleFragment.glsl"];
    //mazeArray = [[NSMutableArray alloc] initWithCapacity:80];
    xDisp = -2.0;
    zDisp = -2.0;
    rotateX = 0.0;
    rotateY = 90.0;
    //MARK: Default cube
//    _cube = [[Cube alloc] initWithShader:_shader];
//    [_cube setPosition:GLKVector3Make(0, -1, 0)];
//    [mazeArray addObject: _cube];
    
    //MARK: Beginning Spinning Cube!
    //_obj = [[ObjModel alloc] initWithPosition:_shader :2 :1.2 :2];
    //[_obj setScale:0.1 :0.1 :0.1];
    //[_obj setFileTexture:@"crate.jpg"];
    
    // PUT IN NAME OF MODEL TEXTURE YOU WANT TO LOAD
    _obj = [[ObjModel alloc] initWithVertices:"obj" textureName:@"mushroom.png" shader:_shader vertices:vert vertexCount: [ObjLoader getVertexCount]];
    //_obj = [[ObjModel alloc] initWithShader:_shader];
    [_obj setPosition:GLKVector3Make(4, 1.1, 1 * 2)];
    [_obj setScale:0.1 :0.1 :0.1];
    //[_obj setRotation:(M_PI_2) :(M_PI) :(0)];
    
    
    _cube = [[Cube alloc] initWithShader:_shader];
    [_cube setPosition:GLKVector3Make(3, 1.5, 1 * 2)];
    [_cube setScale:0.2 :0.2 :0.2];
    [_cube setFileTexture:@"dice.png"];
    //[mazeArray addObject: _cube];
    
    /*_sphere = [[Sphere alloc] initWithShader:_shader];
    [_sphere setPosition:GLKVector3Make(3, 1.5, 1 * 2)];
    [_sphere setScale:0.1 :0.1 :0.1];
    [_sphere setFileTexture:@"crate.jpg"];*/
    

    // HIDE EXTRA FOG CONTROLS
    expFog.hidden = true;
    expFogLabel.hidden = true;
    fogIntensity.hidden = true;
    fogBlue.hidden = true;
    fogGreen.hidden = true;
    fogOrange.hidden = true;
    
    
    
    _shader.projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(40.0), self.view.bounds.size.width / self.view.bounds.size.height, 0.1, 150);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    GLKView *view = (GLKView *)self.view;
    view.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    view.drawableDepthFormat = GLKViewDrawableDepthFormat16;
    
    [EAGLContext setCurrentContext:view.context];
    
    NSError *error;
    vertexCount = 0;
    
    // *****  PUT IN NAME OF MODEL YOU WANT TO LOAD (NO FILE EXTENSION PLEASE)  *********
    [ObjLoader processMtlPath:@"mushroom" objName:@"mushroom" verticesP:&vert error:&error];
    
    if (vert == nil) {
        NSLog(@"Failed to process obj file.\r\n");
    } else {
        NSLog(@"Success!\r\n");
    }
    
    [self->scaleSlider setHidden:true];
    [self->rotXSlider setHidden:true];
    [self->rotYSlider setHidden:true];
    [self->rotZSlider setHidden:true];
    [self->posXSlider setHidden:true];
    [self->posYSlider setHidden:true];
    [self->posZSlider setHidden:true];
    [self setupScene];
    
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    // Sky Colour
    glClearColor(0.2, 0.65, 0.98, 1.0);  // Sky Colour
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    //MARK: Added for extra upf
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    //MARK: RPG VIEW
    GLKMatrix4 viewMatrix = GLKMatrix4MakeTranslation(xDisp, -1.5, zDisp);
    
    //Set our Fog Colour
    fogColor = GLKVector4Make(0.26, 0.70, 0.995, 1.0);  // blue
    //fogColor = GLKVector4Make(0.995, 0.60, 0.26, 1.0);  // orange
    //fogColor = GLKVector4Make(0.26, 0.996, 0.5, 1.0); // green
    
    _shader.fogColour = fogColor;
//
    viewMatrix = GLKMatrix4Translate(viewMatrix, -xDisp, 1.5, -zDisp);
    viewMatrix = GLKMatrix4Rotate(viewMatrix, GLKMathDegreesToRadians(rotateX), 1, 0, 0);
    viewMatrix = GLKMatrix4Rotate(viewMatrix, GLKMathDegreesToRadians(rotateY), 0, 1, 0);
    viewMatrix = GLKMatrix4Translate(viewMatrix, xDisp, -1.5, zDisp);
//
    //MARK: Normal view
//   GLKMatrix4 viewMatrix = GLKMatrix4MakeTranslation(0, 0, -5);
//   viewMatrix = GLKMatrix4Rotate(viewMatrix, GLKMathDegreesToRadians(20), 1, 0, 0);
    
    //MARK: Sizing view
//    GLKMatrix4 viewMatrix = GLKMatrix4MakeTranslation(0, 0, -15);
//    viewMatrix = GLKMatrix4Rotate(viewMatrix, GLKMathDegreesToRadians(30), 1, 0, 0);
    
//    //MARK: Birds eye view
//    GLKMatrix4 viewMatrix = GLKMatrix4MakeTranslation(-8, -8, -25);
//
//    viewMatrix = GLKMatrix4Rotate(viewMatrix, GLKMathDegreesToRadians(60), 1, 0, 0);
//    viewMatrix = GLKMatrix4Rotate(viewMatrix, GLKMathDegreesToRadians(90), 0, 1, 0);
    
    //[_obj renderWithParentModelViewMatrix:viewMatrix];
    [_cube renderWithParentModelViewMatrix:viewMatrix];
    //[_sphere renderWithParentModelViewMatrix:viewMatrix];
    
    
//    MARK: Moves the camera to face downwards
//        viewMatrix = GLKMatrix4Translate(viewMatrix, 0, 0, 0)
//        [_square renderWithParentModelViewMatrix:viewMatrix];
//    [_cube renderWithParentModelViewMatrix:viewMatrix];
    
}

- (void)update {
    [_cube updateWithDelta:self.timeSinceLastUpdate];
    //[_obj updateWithDelta:self.timeSinceLastUpdate :self->array];
    //Cube* p = [mazeArray objectAtIndex: 0];
    //[p updateWithDelta:self.timeSinceLastUpdate];

    
    _shader.fogColour = fogColor;  // FOG COLOUR
}

- (IBAction)PanLeftGesture:(UIPanGestureRecognizer *)sender {
    NSLog(@"Left");
    CGPoint translation = [sender translationInView:self.view];
    float x = translation.x;
    float y = translation.y;
    NSLog(@"X: %f Y: %f", x/100, y/100);
    xDisp += y/1000;
    zDisp += -x/1000;
    
}

- (IBAction)PanRightGesture:(UIPanGestureRecognizer *)sender {
    NSLog(@"Right");
    CGPoint translation = [sender translationInView:self.view];
    float x = translation.x;
    float y = translation.y;
    NSLog(@"X: %f Y: %f", x, y);
    rotateX += y/100;
    rotateY += x/100;
    if (rotateY > 360) {
        rotateY = 0;
    } else if (rotateY < 0) {
        rotateY = 360;
    }
}

- (IBAction)HideShowMaze:(id)sender {
    if (MazeLabel.hidden) {
        MazeLabel.hidden = false;
    } else {
        MazeLabel.hidden = true;
    }
}

- (IBAction)ResetPosition:(id)sender {
    xDisp = -2.0;
    zDisp = -2.0;
    rotateX = 0.0;
    rotateY = 90.0;
}



    
- (IBAction)daytimeOn:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"day on on!");
        _shader.daytime = true;
        _shader.ambientIntensity = 0.9;
    } else {
        NSLog(@"day off!");
        _shader.daytime = false;
        _shader.ambientIntensity = 0.1;
    }
}
    
    
- (IBAction)flashlightOn:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"flashlight on on!");
        _shader.flashlight = true;
        _shader.diffuseIntensity = 1.0;
        _shader.specularIntensity = 0.35;
        _shader.shininess = 10;
    } else {
        NSLog(@"flashlight off!");
        _shader.flashlight = false;
        _shader.diffuseIntensity = 0;
        _shader.specularIntensity = 0;
        _shader.shininess = 0;
    }
}
    
    
- (IBAction)fogOn:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"fog on on!");
        _shader.fog = true;
        expFog.hidden = false;
        expFogLabel.hidden = false;
        fogIntensity.hidden = false;
        fogBlue.hidden = true;
        fogGreen.hidden = true;
        fogOrange.hidden = true;
    } else {
        NSLog(@"fog off!");
        _shader.fog = false;
        expFog.hidden = true;
        expFogLabel.hidden = true;
        fogIntensity.hidden = true;
        fogBlue.hidden = true;
        fogGreen.hidden = true;
        fogOrange.hidden = true;
    }
}



- (IBAction)expFogOn:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"Exp fog on on!");
        _shader.expFog = true;
        //_shader.fogMinDist *= _shader.fogMinDist;
    } else {
        NSLog(@"Exp fog off!");
        _shader.expFog = false;
        _shader.fogMaxDist = 100.0;
    }
}


- (IBAction)fogMakeBlue:(id)sender {
    NSLog(@"Blue clicked!");
    //Set our Fog Colour
    _shader.fogColour = GLKVector4Make(0.26, 0.70, 0.995, 1.0);
}


- (IBAction)fogMakeGreen:(id)sender {
    NSLog(@"White clicked!");
    //Set our Fog Colour
    _shader.fogColour = GLKVector4Make(0.26, 0.996, 0.5, 1.0);
}


- (IBAction)fogMakeOrange:(id)sender {
    NSLog(@"Red clicked!");
    //Set our Fog Colour
    _shader.fogColour = GLKVector4Make(0.995, 0.60, 0.26, 1.0);
}



//- (IBAction)positionEnemy:(id)sender {
//    if (!self->_obj.isMoving) {
//        CGPoint translation = [sender translationInView:self.view];
//        float x = translation.x;
//        float y = translation.y;
////        [glesRenderer translation:translation];
//
//
////        float x = point.x / 100;
////        float y = (-point.y) / 100;
////        xTrans = x;
////        yTrans = y;
//    }
//}



@end

