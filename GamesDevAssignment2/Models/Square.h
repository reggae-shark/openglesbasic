//
//  Square.h
//  GamesDevAssignment2
//
//  Created by Zac Koop on 2018-03-11.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "Model.h"

@interface Square : Model

- (instancetype)initWithShader:(BaseEffect *)shader;

@end

