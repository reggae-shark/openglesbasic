//
//  ObjModel.h
//  GamesDevAssignment2
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "Model.h"

@interface ObjModel : Model

@property (nonatomic) Boolean isMoving;
@property (nonatomic) Boolean isSaved;
@property (nonatomic) GLKVector3 savedPos;
@property (nonatomic) float savedYRot;

- (instancetype)initWithVertices:(char *)name textureName:(NSString *)texture shader:(BaseEffect *)shader vertices:(Vertex [])vertices vertexCount:(unsigned int)vertexCount;
- (instancetype)initWithShader:(BaseEffect *)shader;
- (instancetype)initWithPosition:(BaseEffect *)shader :(float)x :(float)y :(float)z;

//- (void)setPosition:(GLKVector3)position;
- (void)resetObj;
- (void)setMoving:(Boolean)moving;
- (void)setRotation:(float)x :(float)y :(float)z;
- (void)setScale:(float)x :(float)y :(float)z;
- (void)setFileTexture:(NSString *)texture;
- (void)updateWithDelta:(NSTimeInterval)dt :(NSInteger [10][11])mazeArray;

@end


