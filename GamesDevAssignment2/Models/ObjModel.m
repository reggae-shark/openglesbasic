//
//  ObjModel.m
//  GamesDevAssignment2
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//
#import "ObjModel.h"

const Vertex vertices2[] = {
    // Front
    {{1, -1, 1}, {1, 0, 0, 1}, {0.25, 0}, {0, 0, 1}},  // 0
    {{1, 1, 1}, {0, 1, 0, 1}, {0.25, 0.25}, {0, 0, 1}},   // 1
    {{-1, 1, 1}, {0, 0, 1, 1}, {0, 0.25}, {0, 0, 1}},  // 2
    {{-1, -1, 1}, {0, 0, 0, 1}, {0, 0}, {0, 0, 1}}, // 3

    // Back
    {{-1, -1, -1}, {0, 0, 1, 1}, {0.5, 0}, {0, 0, -1}}, // 4
    {{-1, 1, -1}, {0, 1, 0, 1}, {0.5, 0.25}, {0, 0, -1}},  // 5
    {{1, 1, -1}, {1, 0, 0, 1}, {0.25, 0.25}, {0, 0, -1}},   // 6
    {{1, -1, -1}, {0, 0, 0, 1}, {0.25, 0}, {0, 0, -1}},  // 7

    // Left
    {{-1, -1, 1}, {1, 0, 0, 1}, {0.75, 0}, {-1, 0, 0}},  // 8
    {{-1, 1, 1}, {0, 1, 0, 1}, {0.75, 0.25}, {-1, 0, 0}},   // 9
    {{-1, 1, -1}, {0, 0, 1, 1}, {0.5, 0.25}, {-1, 0, 0}},  // 10
    {{-1, -1, -1}, {0, 0, 0, 1}, {0.5, 0}, {-1, 0, 0}}, // 11

    // Right
    {{1, -1, -1}, {1, 0, 0, 1}, {1, 0}, {1, 0, 0}}, // 12
    {{1, 1, -1}, {0, 1, 0, 1}, {1, 0.25}, {1, 0, 0}},  // 13
    {{1, 1, 1}, {0, 0, 1, 1}, {0.75, 0.25}, {1, 0, 0}},   // 14
    {{1, -1, 1}, {0, 0, 0, 1}, {0.75, 0}, {1, 0, 0}},  // 15

    // Top
    {{1, 1, 1}, {1, 0, 0, 1}, {0.25, 0.25}, {0, 1, 0}},   // 16
    {{1, 1, -1}, {0, 1, 0, 1}, {0.25, 0.5}, {0, 1, 0}},  // 17
    {{-1, 1, -1}, {0, 0, 1, 1}, {0, 0.5}, {0, 1, 0}}, // 18
    {{-1, 1, 1}, {0, 0, 0, 1}, {0, 0.25}, {0, 1, 0}},  // 19

    // Bottom
    {{1, -1, -1}, {1, 0, 0, 1}, {0.5, 0.25}, {0, -1, 0}},  // 20
    {{1, -1, 1}, {0, 1, 0, 1}, {0.5, 0.5}, {0, -1, 0}},   // 21
    {{-1, -1, 1}, {0, 0, 1, 1}, {0.25, 0.5}, {0, -1, 0}},  // 22
    {{-1, -1, -1}, {0, 0, 0, 1}, {0.25, 0.25}, {0, -1, 0}}, // 23
};

const GLubyte indices2[] = {
    // Front
    0, 1, 2,
    2, 3, 0,
    // Back
    4, 5, 6,
    6, 7, 4,
    // Left
    8, 9, 10,
    10, 11, 8,
    // Right
    12, 13, 14,
    14, 15, 12,
    // Top
    16, 17, 18,
    18, 19, 16,
    // Bottom
    20, 21, 22,
    22, 23, 20
};

const GLubyte indexList2[6] = {
   0, 1, 2,
   2, 3, 0
};

@implementation ObjModel;

- (instancetype)initWithVertices:(char *)name textureName:(NSString *)texture shader:(BaseEffect *)shader vertices:(Vertex [])vert vertexCount:(unsigned int)vertexCount {
    //NSLog(@"Array size: %d", sizeof(vert) / sizeof(vert[0]));
    NSLog(@"Array size: %d", vertexCount);
    
    if ((self = [super initWithName:name shader:shader vertices:vert vertexCount:vertexCount])) {
        
        [self loadTexture:texture];
        //self.rotationY = M_PI;
        self.rotationX = -1 * M_PI_2;

        self.isMoving = true;
        
    }
    else {
        NSLog(@"NO GO");
    }
    return self;
    
}



- (void)updateWithDelta:(NSTimeInterval)dt :(NSInteger [10][11])mazeArray {
    //self.rotationY += M_PI/8 * dt;
//    printf("isMoving? \n");
    if (_isMoving) {
        printf("Moving \n");
        //is moving
        //starting position is 2,2
        
        // 1 -- 2 -- 3
        // -1 -- 0 -- 1
        
        // 0,4 2,4 4,4
        // 0,2 2,2 4,2
        // 0,0 2,0 4,0
        float speed = 0.05;
        Boolean inMiddle = false;
        int x = 0;
        int z = 0;
//        if ( (int) self.position.x % 3 == 0 || (int) self.position.z % 3 == 0) {
//            inMiddle = true;
//        } else {
            x = [self getInnerCell:self.position.x];
            z = [self getInnerCell:self.position.z];
            printf("VALUE: %i, %i \n", x, z);
//        }
        
        //Y rotation is the direction that the NPC is moving forward in
        if (self.rotationY == 0) {
            //moving forward
            Boolean colide = false;
            if (!inMiddle) {
                //2,4
                NSInteger cellType = mazeArray[x + 1][z]; //cell infront of npc point of view
                if (cellType == 1) {
                    printf("FRONT Cell: X: %i Y: %i,  Type: %i \n", x + 1, z, (int) cellType);
                    //detect collision
                    if ((x * 2 + 1) - self.position.x <= 0.1) {
                        // z * 2 + 1
                        // 1 * 2 + 1 = 3 check position
                        self.position = GLKVector3Make(self.position.x - 0.3, self.position.y, self.position.z);
                        colide = true;
                    }
                } else {
                    colide = false;
                }
            }
            
            //move if true
            if (!colide) {
                //continue to move forward
                printf("Moving Unit Forward \n");
                self.position = GLKVector3Make(self.position.x + speed,self.position.y,self.position.z);
            } else {
                //DID colide!
                int i = arc4random_uniform(4);
                self.rotationY = i * 90;
            }
        } else if (self.rotationY == 90) {
            //moving left
            Boolean colide = false;
            if (!inMiddle) {
                //4,2
                NSInteger cellType = mazeArray[x][z + 1]; //cell infront of npc point of view
                if (cellType == 1) {
                    printf("LEFT Cell: X: %i Y: %i,  Type: %i \n", x, z + 1, (int) cellType);
                    //detect collision
                    if ((z * 2 + 1) - self.position.z <= 0.1) {
                        // z * 2 + 1
                        // 1 * 2 + 1 = 3 check position
                        self.position = GLKVector3Make(self.position.x, self.position.y, self.position.z - 0.3);
                        colide = true;
                    }
                } else {
                    colide = false;
                }
            }
            
            //move if true
            if (!colide) {
                //continue to move forward
                printf("Moving Unit Forward \n");
                self.position = GLKVector3Make(self.position.x, self.position.y, self.position.z + speed);
            } else {
                //DID colide!
                int i = arc4random_uniform(4);
                self.rotationY = i * 90;
            }
        } else if (self.rotationY == 180) {
            //moving backward
            Boolean colide = false;
            if (!inMiddle) {
                //2,0
                NSInteger cellType = mazeArray[x - 1][z]; //cell infront of npc point of view
                if (cellType == 1) {
                    printf("BACK Cell: X: %i Y: %i,  Type: %i \n", x - 1, z, (int) cellType);
                    //detect collision
                    if (self.position.x - (x * 2 - 1) <= 0.1) {
                        // z * 2 + 1
                        // 1 * 2 + 1 = 3 check position
                        self.position = GLKVector3Make(self.position.x + 0.3, self.position.y, self.position.z);
                        colide = true;
                    }
                } else {
                    colide = false;
                }
            }
            
            //move if true
            if (!colide) {
                //continue to move forward
                printf("Moving Unit Forward \n");
                self.position = GLKVector3Make(self.position.x - speed, self.position.y, self.position.z);
            } else {
                //DID colide!
                int i = arc4random_uniform(4);
                self.rotationY = i * 90;
            }
        } else if (self.rotationY == 270) {
            //moving right
            Boolean colide = false;
            if (!inMiddle) {
                //0,2
                NSInteger cellType = mazeArray[x][z - 1]; //cell infront of npc point of view
                if (cellType == 1) {
                    printf("RIGHT Cell: X: %i Y: %i,  Type: %i \n", x, z - 1, (int) cellType);
                    //detect collision
                    if (self.position.z - (z * 2 - 1) <= 0.1) {
                        // z * 2 + 1
                        // 1 * 2 + 1 = 3 check position
                        self.position = GLKVector3Make(self.position.x, self.position.y, self.position.z + 0.3);
                        colide = true;
                    }
                } else {
                    colide = false;
                }
            }
            
            //move if true
            if (!colide) {
                //continue to move forward
                printf("Moving Unit Forward \n");
                self.position = GLKVector3Make(self.position.x, self.position.y, self.position.z - speed);
            } else {
                //DID colide!
                int i = arc4random_uniform(4);
                self.rotationY = i * 90;
            }
        }
    } else {
        //not moving
        if (!self.isSaved) {
            self.savedPos = self.position;
            self.savedYRot = self.rotationY;
            self.isSaved = true;
        }
    }
    

}

- (int)getInnerCell:(float) value {
//    int tmp = (int) value;
//    float upper = (tmp + 1) - value;
//    float lower = value - (tmp - 1);
//    printf("upper %f \n", upper);
//    printf("lower %f \n", lower);
//
//    if (upper < 1) {
//        printf(" %i \n", tmp + 1);
//        return upper;
//    } else if (lower < 1) {
//        printf(" %i \n", tmp - 1);
//        return upper;
//    } else if (upper == 1 && lower == 1) {
//        printf("failed");
//        return (int) -1;
//    }
//
//    return -1;
    return (int) roundf(value / 2.0f);
}

- (void)resetObj {
    [self setRotation:0 :self.savedYRot :0];
    [self setPosition:GLKVector3Make(self.savedPos.x, 1, self.savedPos.z)];
    [self setScale:0.1 :0.1 :0.1];
}

- (void)setMoving:(Boolean)moving {
    self.isMoving = moving;
}

- (void)setRotation:(float)x :(float)y :(float)z {
    self.rotationX = x;
    self.rotationY = y;
    self.rotationZ = z;
}

- (void)setScale:(float)x :(float)y :(float)z {
    self.scaleX = x;
    self.scaleY = y;
    self.scaleZ = z;
}

- (void)setFileTexture:(NSString *)texture {
    [self loadTexture:texture];
}


@end


