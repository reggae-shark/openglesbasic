//
//  Sphere.h
//  GamesDevAssignment2
//
//  Created by Zac Koop on 2018-04-11.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "Model.h"

@interface Sphere : Model

- (instancetype)initWithShader:(BaseEffect *)shader;
- (instancetype)initWithPosition:(BaseEffect *)shader :(float)x :(float)y :(float)z;

//- (void)setPosition:(GLKVector3)position;
- (void)setRotation:(float)x :(float)y :(float)z;
- (void)setScale:(float)x :(float)y :(float)z;
- (void)setFileTexture:(NSString *)texture;

@end
