//
//  main.m
//  GamesDevAssignment2
//
//  Created by Dalton Danis on 2018-03-07.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
