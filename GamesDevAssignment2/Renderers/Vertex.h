//
//  Vertex.h
//  GamesDevAssignment2
//
//  Created by Dalton Danis on 2018-03-11.
//  Copyright © 2018 opengl. All rights reserved.
//
@import GLKit;

typedef enum {
    VertexAttribPosition = 0,
    VertexAttribColor,
    VertexAttribTexCoord,
    VertexAttribNormal
} VertexAttributes;

typedef struct {
    GLfloat Position[3];
    GLfloat Color[4];
    GLfloat TexCoord[2];
    GLfloat Normal[3];
} Vertex;
