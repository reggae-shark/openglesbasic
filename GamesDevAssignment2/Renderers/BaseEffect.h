//
//  BaseEffect.h
//  GamesDevAssignment2
//
//  Created by Dalton Danis on 2018-03-11.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GLKit;

@interface BaseEffect : NSObject
    
    @property (nonatomic, assign) GLuint programHandle;
    @property (assign) GLKMatrix4 modelMatrix;
    @property (nonatomic, assign) GLKMatrix4 modelViewMatrix;
    @property (nonatomic, assign) GLKMatrix4 projectionMatrix;
    @property (assign) GLuint texture;
    @property (assign) GLuint colorUniform;
    //var colorUniform : Int32 = 0
    
    @property (assign) GLKVector3 lightColor;
    @property (assign) GLfloat ambientIntensity;
    @property (assign) GLfloat diffuseIntensity;
    @property (assign) GLfloat specularIntensity;
    @property (assign) GLKVector3 lightDirection;
    @property (assign) float shininess;
    @property (assign) GLfloat fogMaxDist;
    @property (assign) GLfloat fogMinDist;
    @property (assign) GLboolean daytime;
    @property (assign) GLboolean flashlight;
    @property (assign) GLboolean fog;
    @property (assign) GLboolean expFog;
    @property (assign) GLKVector4 fogColour;
    
- (id)initWithVertexShader:(NSString *)vertexShader
            fragmentShader:(NSString *)fragmentShader;
- (void)prepareToDraw;
    
    @end

