//
//  BaseEffect.c
//  GamesDevAssignment2
//
//  Created by Dalton Danis on 2018-03-11.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "BaseEffect.h"
#import "Vertex.h"

@implementation BaseEffect {
    GLuint _programHandle;
    GLuint _modelViewMatrixUniform;
    GLuint _projectionMatrixUniform;
    GLuint _texUniform;
    GLuint _colorUniform;
    GLuint _lightColorUniform;
    GLuint _lightAmbientIntensityUniform;
    GLuint _lightDiffuseIntensityUniform;
    GLuint _lightDirectionUniform;
    GLuint _matSpecularIntensityUniform;
    GLuint _shininessUniform;
    GLuint _fogMaxDistUniform;
    GLuint _fogMinDistUniform;
    GLuint _daytimeUniform;
    GLuint _flashlightUniform;
    GLuint _fogUniform;
    GLuint _fogColourUniform;
    GLuint _expFogUniform;
}

- (GLuint)compileShader:(NSString*)shaderName withType:(GLenum)shaderType {
    NSString* shaderPath = [[NSBundle mainBundle] pathForResource:shaderName ofType:nil];
    NSError* error;
    NSString* shaderString = [NSString stringWithContentsOfFile:shaderPath encoding:NSUTF8StringEncoding error:&error];
    if (!shaderString) {
        NSLog(@"Error loading shader: %@", error.localizedDescription);
        exit(1);
    }
    
    GLuint shaderHandle = glCreateShader(shaderType);
    
    const char * shaderStringUTF8 = [shaderString UTF8String];
    int shaderStringLength = [shaderString length];
    glShaderSource(shaderHandle, 1, &shaderStringUTF8, &shaderStringLength);
    
    glCompileShader(shaderHandle);
    
    GLint compileSuccess;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
    if (compileSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"%@", messageString);
        exit(1);
    }
    
    return shaderHandle;
}

- (void)compileVertexShader:(NSString *)vertexShader
             fragmentShader:(NSString *)fragmentShader {
    GLuint vertexShaderName = [self compileShader:vertexShader
                                         withType:GL_VERTEX_SHADER];
    GLuint fragmentShaderName = [self compileShader:fragmentShader
                                           withType:GL_FRAGMENT_SHADER];
    
    _programHandle = glCreateProgram();
    glAttachShader(_programHandle, vertexShaderName);
    glAttachShader(_programHandle, fragmentShaderName);
    
    glBindAttribLocation(_programHandle, VertexAttribPosition, "a_Position");
    glBindAttribLocation(_programHandle, VertexAttribColor, "a_Color");
    glBindAttribLocation(_programHandle, VertexAttribTexCoord, "a_TexCoord");
    glBindAttribLocation(_programHandle, VertexAttribNormal, "a_Normal");
    
    glLinkProgram(_programHandle);
    
    self.modelViewMatrix = GLKMatrix4Identity;
    _modelViewMatrixUniform = glGetUniformLocation(_programHandle, "u_ModelViewMatrix");
    _projectionMatrixUniform = glGetUniformLocation(_programHandle, "u_ProjectionMatrix");
    _texUniform = glGetUniformLocation(_programHandle, "u_Texture");
    _colorUniform = glGetUniformLocation(_programHandle, "u_SurfaceColor");
    _lightColorUniform = glGetUniformLocation(_programHandle, "u_Light.Color");
    _lightAmbientIntensityUniform = glGetUniformLocation(_programHandle, "u_Light.AmbientIntensity");
    _lightDiffuseIntensityUniform = glGetUniformLocation(_programHandle, "u_Light.DiffuseIntensity");  // Diffuse
    _lightDirectionUniform = glGetUniformLocation(_programHandle, "u_Light.Direction");    // Diffuse
    _matSpecularIntensityUniform = glGetUniformLocation(_programHandle, "u_MatSpecularIntensity");   // Specular
    _shininessUniform = glGetUniformLocation(_programHandle, "u_Shininess");   // Specular
    _fogMaxDistUniform = glGetUniformLocation(_programHandle, "u_fogMaxDist");  // max fog distance
    _fogMinDistUniform = glGetUniformLocation(_programHandle, "u_fogMaxDist"); // min fog distance
    
    _daytimeUniform = glGetUniformLocation(_programHandle, "u_daytime");   // daytime boolean uniform
    _flashlightUniform = glGetUniformLocation(_programHandle, "u_flashlight");   // flashlight boolean uniform
    _fogUniform = glGetUniformLocation(_programHandle, "u_fog");   // fog boolean uniform
    _expFogUniform = glGetUniformLocation(_programHandle, "u_expFog");   // exponential fog boolean uniform
    
    
    _fogColourUniform = glGetUniformLocation(_programHandle, "u_fogColour");   // Fog colour
    
    GLint linkSuccess;
    glGetProgramiv(_programHandle, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetProgramInfoLog(_programHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"%@", messageString);
        exit(1);
    }
}

- (void)prepareToDraw {
    glUseProgram(_programHandle);
    glUniformMatrix4fv(_modelViewMatrixUniform, 1, 0, self.modelViewMatrix.m);
    glUniformMatrix4fv(_projectionMatrixUniform, 1, 0, self.projectionMatrix.m);
    
    glUniform3f(_lightColorUniform, self.lightColor.r, self.lightColor.g, self.lightColor.b);
    glUniform1f(_lightAmbientIntensityUniform, self.ambientIntensity);
    
    // Diffuse
    glUniform3f(_lightDirectionUniform, self.lightDirection.x, self.lightDirection.y, self.lightDirection.z);
    glUniform1f(_lightDiffuseIntensityUniform, self.diffuseIntensity);  // Diffuse
    // Specular
    glUniform1f(_matSpecularIntensityUniform, self.specularIntensity);  // Specular
    glUniform1f(_shininessUniform, self.shininess);  // Specular
    
    // Fog
    //glUniform1f(_fogMaxDistUniform, self.fogMaxDist);  // Fog max distance
    //glUniform1f(_fogMaxDistUniform, self.fogMinDist);  // Fog min distance
    glUniform1f(_fogMaxDistUniform, self.fogMaxDist);  // Fog max distance
    glUniform1f(_fogMaxDistUniform, self.fogMinDist);  // Fog min distance
    glUniform4f(_fogColourUniform, self.fogColour.r, self.fogColour.g, self.fogColour.b, 1.0);   // Fog colour
    
    glUniform1f(_daytimeUniform, self.daytime);  // day boolean
    glUniform1f(_flashlightUniform, self.flashlight);  // flashlight boolean
    glUniform1f(_fogUniform, self.fog);  // fog boolean
    glUniform1f(_expFogUniform, self.expFog);  // exp.fog boolean
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, self.texture);
    glUniform1i(_texUniform, 1);
    
}

- (instancetype)initWithVertexShader:(NSString *)vertexShader fragmentShader:
(NSString *)fragmentShader {
    if ((self = [super init])) {
        self.lightColor = GLKVector3Make(1, 1, 1);
        self.lightDirection = GLKVector3Normalize(GLKVector3Make(0, 0.17, -1));
        self.ambientIntensity = 0.7;
        self.diffuseIntensity = 0;
        self.specularIntensity = 0;
        self.shininess = 0;
        self.fogMaxDist = 100;
        self.fogMinDist = 2.5;
        self.daytime = true;
        self.flashlight = false;
        self.fog = false;
        
        [self compileVertexShader:vertexShader fragmentShader:fragmentShader];
    }
    return self;
}

@end
