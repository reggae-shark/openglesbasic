//
//  ModelObject.m
//  ObjLoader
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "ModelObject.h"

@implementation ModelObject

- (id)init {
    if ((self = [super init])) {
        self.name = nil;
        self.vertices = [NSMutableArray array];
        self.textureCoords = [NSMutableArray array];
        self.normals = [NSMutableArray array];
        self.modelObjectParts = [NSMutableArray array];
    }
    return self;
}


@end
