//
//  TriangleSet.m
//  ObjLoader
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "ModelObjectPart.h"

@implementation ModelObjectPart

- (id)init {
    if ((self = [super init])) {
        self.material = nil;
        self.vertexInfos = [NSMutableArray array];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"[%@, %@]", _material, _vertexInfos];
}

@end
