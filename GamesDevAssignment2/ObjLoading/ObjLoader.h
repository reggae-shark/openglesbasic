//
//  ObjLoader.h
//  ObjLoader
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vertex.h"

@interface ObjLoader : NSObject

+ (void)processMtlPath:(NSString *)mtlName objName:(NSString *)objName verticesP:(Vertex **)verticesP error:(NSError **)error;
+ (long)getVertexCount;
//@property (nonatomic, assign) int vertexCount;

@end
