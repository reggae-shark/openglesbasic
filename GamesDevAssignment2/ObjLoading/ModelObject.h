//
//  ModelObject.h
//  ObjLoader
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelObjectPart.h"
#import "Vector3.h"
#import "Vector2.h"

@interface ModelObject : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray *vertices;
@property (nonatomic, strong) NSMutableArray *textureCoords;
@property (nonatomic, strong) NSMutableArray *normals;
@property (nonatomic, strong) NSMutableArray *modelObjectParts;

@end
