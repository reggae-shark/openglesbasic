//
//  Vector3.h
//  ObjLoader
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vector3 : NSObject

@property (nonatomic, assign) float x;
@property (nonatomic, assign) float y;
@property (nonatomic, assign) float z;

@end
