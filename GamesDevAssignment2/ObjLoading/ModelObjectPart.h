//
//  TriangleSet.h
//  ObjLoader
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VertexInfo.h"
#import "Material.h"

@interface ModelObjectPart : NSObject

@property (nonatomic, strong) Material *material;
@property (nonatomic, strong) NSMutableArray *vertexInfos;

@end
