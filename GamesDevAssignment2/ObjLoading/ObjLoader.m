//
//  ObjLoader.m
//  ObjLoader
//
//  Created by Zac Koop on 2018-04-03.
//  Copyright © 2018 opengl. All rights reserved.
//

#import "ObjLoader.h"
#import "Material.h"
#import "ModelObject.h"

@implementation ObjLoader

long vertexCount;

+ (long)getVertexCount {
    
    return vertexCount;
}


+ (void)processMtlPath:(NSString *)mtlName objName:(NSString *)objName verticesP:(Vertex **)verticesP error:(NSError **)error {
    
    NSStringEncoding encoding;
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath = [mainBundle pathForResource:mtlName ofType:@"mtl"];
    
    NSString *mtlString = [[NSString alloc] initWithContentsOfFile:filePath usedEncoding:&encoding error:error];
    if (!mtlString)  {
        NSLog(@"mtl failed");
        //return nil;
    }
    
    filePath = [mainBundle pathForResource:objName ofType:@"obj"];
    NSString *objString = [[NSString alloc] initWithContentsOfFile:filePath usedEncoding:&encoding error:error];
    if (!objString) {
        NSLog(@"obj failed");
        //return nil;
    }
    
    NSLog(@"MTL");
    NSLog(@"----------------------------------------");
    NSLog(@"%@", mtlString);
    
    NSDictionary *materials = [self materialsFromString:mtlString];
    //NSLog(@"Materials: %@", materials);
    
    //NSLog(@"OBJ");
    //NSLog(@"---");
    //NSLog(@"%@", objString);
    
    NSDictionary *objects = [self objectsFromString:objString materials:materials];
    //NSLog(@"Objects: %@", objects);
    
    [self implementationFromObjects:objects objName:objName verticesP:verticesP];
    
    if (verticesP == nil) {
        NSLog(@"Model Loading Failed.  Vertex* is null. ");
    }
    
    //return verticesP;
}



+ (NSDictionary *)materialsFromString:(NSString *)mtlString {
    
    Material *material = nil;
    NSMutableDictionary * materials = [NSMutableDictionary dictionary];
    
    NSArray *lines = [mtlString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString * line in lines) {
        
        NSArray *components = [line componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (components.count < 1) continue;
        
        NSString *command = (NSString *) components[0];
        if ([command isEqualToString:@"newmtl"]) {
            if (components.count < 2) continue;
            if (material) {
                [materials setObject:material forKey:material.name];
            }
            material = [[Material alloc] init];
            material.name = [components[1] stringByReplacingOccurrencesOfString:@"." withString:@"_"];
        }
        if ([command isEqualToString:@"Ns"]) {
            if (components.count < 2) continue;
            material.shininess = [components[1] floatValue];
        }
        if ([command isEqualToString:@"Ka"]) {
            if (components.count < 4) continue;
            material.ambientR = [components[1] floatValue];
            material.ambientG = [components[2] floatValue];
            material.ambientB = [components[3] floatValue];
        }
        if ([command isEqualToString:@"Kd"]) {
            if (components.count < 4) continue;
            material.diffuseR = [components[1] floatValue];
            material.diffuseG = [components[2] floatValue];
            material.diffuseB = [components[3] floatValue];
        }
        if ([command isEqualToString:@"Ks"]) {
            if (components.count < 4) continue;
            material.specularR = [components[1] floatValue];
            material.specularG = [components[2] floatValue];
            material.specularB = [components[3] floatValue];
        }
        if ([command isEqualToString:@"d"]) {
            if (components.count < 2) continue;
            material.alpha = [components[1] floatValue];
        }
    }
    if (material) {
        [materials setObject:material forKey:material.name];
    }
    
    return materials;
    
}



+ (NSDictionary *)objectsFromString:(NSString *)objectString materials:(NSDictionary *)materials {
    
    ModelObject *modelObject = nil;
    ModelObjectPart *modelObjectPart = nil;
    NSMutableDictionary * modelObjects = [NSMutableDictionary dictionary];
    
    NSArray *lines = [objectString componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString * line in lines) {
        
        NSArray *components = [line componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (components.count < 1) continue;
        
        NSString *command = components[0];
        if ([command isEqualToString:@"o"]) {
            if (components.count < 2) continue;
            if (modelObject) {
                [modelObjects setObject:modelObject forKey:modelObject.name];
            }
            modelObject = [[ModelObject alloc] init];
            modelObject.name = [components[1] stringByReplacingOccurrencesOfString:@"." withString:@"_"];
            NSLog(@"***********************");
            NSLog(@"ModelObject Name: %@", modelObject.name);
        }
        if ([command isEqualToString:@"v"]) {
            if (components.count < 4) continue;
            Vector3 *vector = [[Vector3 alloc] init];
            vector.x = [components[1] floatValue];
            vector.y = [components[2] floatValue];
            vector.z = [components[3] floatValue];
            [modelObject.vertices addObject:vector];
        }
        if ([command isEqualToString:@"vt"]) {
            if (components.count < 3) continue;
            Vector2 *vector = [[Vector2 alloc] init];
            vector.x = [components[1] floatValue];
            vector.y = [components[2] floatValue];
            [modelObject.textureCoords addObject:vector];
        }
        if ([command isEqualToString:@"vn"]) {
            if (components.count < 4) continue;
            Vector3 *vector = [[Vector3 alloc] init];
            vector.x = [components[1] floatValue];
            vector.y = [components[2] floatValue];
            vector.z = [components[3] floatValue];
            [modelObject.normals addObject:vector];
        }
        if ([command isEqualToString:@"usemtl"]) {
            if (components.count < 2) continue;
            NSString *materialName = components[1];
            if (modelObjectPart) {
                [modelObject.modelObjectParts addObject:modelObjectPart];
            }
            modelObjectPart = [[ModelObjectPart alloc] init];
            modelObjectPart.material = materials[materialName];
        }
        if ([command isEqualToString:@"f"]) {
            if (components.count < 4) continue;
            [modelObjectPart.vertexInfos addObject:[[VertexInfo alloc] initWithString:components[1]]];
            [modelObjectPart.vertexInfos addObject:[[VertexInfo alloc] initWithString:components[2]]];
            [modelObjectPart.vertexInfos addObject:[[VertexInfo alloc] initWithString:components[3]]];
        }
    }
    
    if (modelObjectPart) {
        [modelObject.modelObjectParts addObject:modelObjectPart];
    }
    if (modelObject) {
        [modelObjects setObject:modelObject forKey:modelObject.name];
    }
    
    return modelObjects;
    
}



// THIS IS THE METHOD WE'RE DEBUGGING.  GETS CALLED AND VERTEX ** PASSED IN BY processMtlPath (line 23)
+ (void)implementationFromObjects:(NSDictionary *)objects objName:(NSString *)objName verticesP:(Vertex **)verticesP {
    
    NSLog(@"---------------------------------");
    NSLog(@"Obj Name:  %@", objName);
    
    ModelObject *modelObject = objects[objName];
    if (!modelObject) {
        NSLog(@"Model Object dictionary Key not compatible!!");
        //return nil;
    }
    
    ModelObjectPart *modelObjectPart = modelObject.modelObjectParts[0];
    vertexCount = [modelObjectPart.vertexInfos count];
    NSLog(@"COUNT: %d", (int)vertexCount);
    
    NSMutableData* data = [NSMutableData dataWithLength:sizeof(Vertex) * (int)vertexCount];
    *verticesP = [data mutableBytes];
    //*verticesP = (Vertex *)malloc(vertexCount * sizeof(Vertex));
    // Vertex vertices[vertexCount];
    Vertex *vertices = [data mutableBytes];
    // *verticesP = (Vertex *)malloc(sizeof(vertices));
    // NSLog(@"Number of vertexInfos:  %d", vertexCount);
    NSLog(@"Size of Vertex:  %d", sizeof(Vertex));
    // NSLog(@"Mallocating Num * Size of Vertex:  %d", vertexCount * sizeof(Vertex));
    
    for (ModelObject *modelObject in objects.allValues) {
        for (ModelObjectPart *modelObjectPart in modelObject.modelObjectParts) {
            
            int i = 0;
            for(VertexInfo *vertexInfo in modelObjectPart.vertexInfos) {
                
                Vector3 * position = [modelObject.vertices objectAtIndex:vertexInfo.vertexIdx - 1];
                Vector2 * texCoord = [modelObject.textureCoords objectAtIndex:vertexInfo.textureCoordIdx - 1];
                Vector3 * normal = [modelObject.normals objectAtIndex:vertexInfo.normalIdx - 1];
                
                Vertex vertex;
                vertex.Position[0] = position.x;
                vertex.Position[1] = position.y;
                vertex.Position[2] = position.z;
                for (int i = 0; i < 4; i++) {
                    vertex.Color[i] = 1;
                }
                vertex.TexCoord[0] = texCoord.x;
                vertex.TexCoord[1] = texCoord.y;
                vertex.Normal[0] = normal.x;
                vertex.Normal[1] = normal.y;
                vertex.Normal[2] = normal.z;
                
                
                vertices[i] = vertex;
                i++;
            }
        }
        verticesP = &vertices;
    }
    
    /*for (int i = 0; i < vertexCount; ++i) {
        //NSLog (@"I IS %d", i);
        //float temp = verticesP[0][i].Position[0];
        NSLog(@"\n    {{%f, %f, %f}, {1, 1, 1, 1}, {%f, %f}, {%f, %f, %f}},", verticesP[0][i].Position[0], verticesP[0][i].Position[1], verticesP[0][i].Position[2], verticesP[0][i].TexCoord[0], verticesP[0][i].TexCoord[1], vertices[i].Normal[0], verticesP[0][i].Normal[1], verticesP[0][i].Normal[2]);
        //NSLog (@"%.2f", temp);
    }*/
}


@end

