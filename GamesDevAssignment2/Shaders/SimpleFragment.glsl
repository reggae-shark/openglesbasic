varying lowp vec4 frag_Color;
varying lowp vec2 frag_TexCoord;
varying lowp vec3 frag_Normal;      // diffuse
varying lowp vec3 frag_Position;    // Specular
varying highp float frag_eyeDist;

uniform sampler2D u_Texture;
uniform highp float u_MatSpecularIntensity;   // Specular
uniform highp float u_Shininess;    // Specular
uniform bool u_daytime;
uniform bool u_flashlight;
uniform bool u_fog;
uniform bool u_expFog;
uniform lowp vec4 u_fogColour;

uniform highp float u_fogMaxDist;  // Fog max distance
uniform highp float u_fogMinDist;  // Fog min distance

struct Light {
    lowp vec3 Color;
    lowp float AmbientIntensity;
    lowp float DiffuseIntensity;
    lowp vec3 Direction;
};
uniform Light u_Light;

// Calculates linear fog factor
highp float computeLinearFogFactor() {
    highp float factor;
    
    // Compute linear fog equation
    
    factor = (u_fogMaxDist - frag_eyeDist) /
    (u_fogMaxDist - u_fogMinDist );
    
    // Clamp in the [0,1] range
    factor = clamp(factor, 0.0, 1.0 );
    
    return factor;
}

void main(void) {
    
    // Ambient
    lowp vec3 AmbientColor = u_Light.Color * u_Light.AmbientIntensity;
    
    
    // Diffuse
    lowp vec3 Normal = normalize(frag_Normal);
    lowp float DiffuseFactor = max(-dot(Normal, u_Light.Direction), 0.0);   // Diffuse
    //lowp float DiffuseFactor = -dot(normalize(Normal), normalize(u_Light.Direction));
    lowp vec3 DiffuseColor = u_Light.Color * u_Light.DiffuseIntensity * DiffuseFactor;  // Diffuse
    
    // Specular
    lowp vec3 Eye = normalize(frag_Position);
    lowp vec3 Reflection = reflect(u_Light.Direction, Normal);
    lowp float SpecularFactor = pow(max(0.0, -dot(Reflection, Eye)), u_Shininess);   // Specular
    lowp vec3 SpecularColor = u_Light.Color * u_MatSpecularIntensity * SpecularFactor;  // Specular
    
    
    // Fog stuff
    lowp float spotlightCutoff = 0.988; // cos(spotlight FOV / 2)
    //lowp float spotlightCutoff = 0.9961; // cos(spotlight FOV / 2)
    highp float fogFactor = computeLinearFogFactor();
    lowp vec4 fogColor = (1.0 - fogFactor) * u_fogColour;
    //fogColor.alpha = 1.0;
    lowp vec4 baseColor = texture2D(u_Texture, frag_TexCoord);
    
    // * spotlight value - spotlightCutoff / (1.0 - spotlightCutoff)
    lowp float spotlightValue = dot(normalize(frag_Position), vec3(0.0,0.0,-1.0));
    
    lowp float flashlightFade = clamp((spotlightValue - spotlightCutoff) / (1.0 - spotlightCutoff), 0.0, 1.0);
    
    if (u_expFog) {
        // (1.0 - f)*fogColor + f * lightColor
        fogFactor = pow(fogFactor, 0.3);
        clamp(fogFactor, 0.0, 1.0 );
    }
    
    if (u_fog) {
        if (u_flashlight && spotlightValue > spotlightCutoff) {  //flashlight on
            gl_FragColor = baseColor * fogFactor * vec4((AmbientColor + DiffuseColor + SpecularColor), 1.0) + fogColor;
        }
        else {  //no flashlight
            gl_FragColor = baseColor * fogFactor * vec4(AmbientColor, 1.0) + fogColor;
        }
    }
    else {  // no fog
        if (u_flashlight && spotlightValue > spotlightCutoff) {  // flashlight on
            gl_FragColor = baseColor * vec4((AmbientColor + DiffuseColor + SpecularColor), 1.0);
        }
        else {
            gl_FragColor = baseColor * vec4(AmbientColor, 1.0);
        }
    }
//    gl_FragColor = texture2D(u_Texture, frag_TexCoord);
}
