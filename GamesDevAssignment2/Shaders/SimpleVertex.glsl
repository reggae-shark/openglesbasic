uniform highp mat4 u_ModelViewMatrix;
uniform highp mat4 u_ProjectionMatrix;
uniform vec4 u_SurfaceColor;
uniform bool u_fog;  // is fog on?
uniform bool u_expFog;  // exponential fog


attribute vec4 a_Position;
attribute vec4 a_Color;
attribute vec2 a_TexCoord;
attribute vec3 a_Normal;         // for diffuse lighting


varying lowp vec4 frag_Color;
varying lowp vec2 frag_TexCoord;
varying lowp vec3 frag_Normal;    // for diffuse lighting
varying lowp vec3 frag_Position;  // for specular lighting
varying highp float frag_eyeDist;  // eye distance

void main(void) {
    
    //frag_Color = a_Color;
    frag_Color = u_SurfaceColor;
    gl_Position = u_ProjectionMatrix * u_ModelViewMatrix * a_Position;
    frag_TexCoord = a_TexCoord;
    frag_Normal = (u_ModelViewMatrix * vec4(a_Normal, 0.0)).xyz;      // for diffuse lighting  (to camera coordinates)
    frag_Position = (u_ModelViewMatrix * a_Position).xyz;  // for specular lighting (to camera coordinates)
    
    if (u_fog) {
        lowp vec3 Eye = normalize(frag_Position);   // Gets the Eye position vector
        vec4 ViewingPos = u_ProjectionMatrix * u_ModelViewMatrix * a_Position;  // Gets the position of whatever we are viewing
        
        // For Fog: Computes the distance to eye using pythagoras on the eye vector and viewing position vector
        frag_eyeDist = sqrt( (ViewingPos.x - Eye.x) *
                            (ViewingPos.x - Eye.x) +
                            (ViewingPos.y - Eye.y) *
                            (ViewingPos.y - Eye.y) +
                            (ViewingPos.z - Eye.z) *
                            (ViewingPos.z - Eye.z) );
    }
}
